import React, { Component } from 'react';
import axios from 'axios';

export default class Create extends Component {
  constructor(props) {
    super(props);
    this.onChangePersonName = this.onChangePersonName.bind(this);
    this.onChangeBusinessEmail = this.onChangeBusinessEmail.bind(this);
    this.onChangePhoneNumber = this.onChangePhoneNumber.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      person_name: '',
      business_email: '',
      business_phone_number:''
    }
  }
  onChangePersonName(param) {
    this.setState({
      person_name: param.target.value
    });
  }
  onChangeBusinessEmail(param) {
    this.setState({
      business_email: param.target.value
    })  
  }
  onChangePhoneNumber(param) {
    this.setState({
      business_phone_number: param.target.value
    })
  }

  onSubmit(param) {
    param.preventDefault();

    

    const obj =  {
      person_name: this.state.person_name,
      business_email: this.state.business_email,
      business_phone_number: this.state.business_phone_number
      
    };

    this.setState({
      person_name: '',
      business_email: '',
      business_phone_number: ''
    })
axios.post('http://localhost:4000/create/add', obj)
        .then(res => console.log(res.obj));
        
   
  }
 
  render() {
    return (
        <div style={{ marginTop: 10 }}>
            <h3>Add </h3>
            <form onSubmit={this.onSubmit}>
                <div className="form-group">
                    <label>Person Name:  </label>
                    <input 
                      type="text" 
                      className="form-control" 
                      value={this.state.person_name}
                      onChange={this.onChangePersonName}
                      />
                </div>
                <div className="form-group">
                    <label>Email: </label>
                    <input type="text" 
                      className="form-control"
                      value={this.state.business_email}
                      onChange={this.onChangeBusinessEmail}
                      />
                </div>
                <div className="form-group">
                    <label>Phone Number: </label>
                    <input type="text" 
                      className="form-control"
                      value={this.state.business_phone_number}
                      onChange={this.onChangePhoneNumber}
                      />
                </div>
                <div className="form-group">
                    <input  type="submit" value="Register Business" className="btn btn-primary"/>
                </div>
            </form>
        </div>
    )
  }
}