import React, { Component } from 'react';
import axios from 'axios';

export default class Edit extends Component {
  constructor(props) {
    super(props);
    this.onChangePersonName = this.onChangePersonName.bind(this);
    this.onChangeBusinessEmail = this.onChangeBusinessEmail.bind(this);
    this.onChangePhoneNumber = this.onChangePhoneNumber.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      person_name: '',
      business_email: '',
      business_phone_number:''
    }
  }

  componentDidMount() {
      axios.get('http://localhost:4000/business/edit/'+this.props.match.params.id)
          .then(response => {
              this.setState({ 
                person_name: response.data.person_name, 
                business_email: response.data.business_email,
                business_phone_number: response.data.business_phone_number });
          })
          .catch(function (error) {
              console.log(error);
          })
    }

  onChangePersonName(param) {
    this.setState({
      person_name: param.target.value
    });
  }
  onChangeBusinessEmail(param) {
    this.setState({
      business_email: param.target.value
    })  
  }
  onChangePhoneNumber(param) {
    this.setState({
      business_phone_number: param.target.value
    })
  }

  onSubmit(param) {
    param.preventDefault();
    const obj = {
      person_name: this.state.person_name,
      business_email: this.state.business_email,
      business_phone_number: this.state.business_phone_number
    };
    axios.post('http://localhost:4000/business/update/'+this.props.match.params.id, obj)
        .then(res => console.log(res.data));
    
    this.props.history.push('/index');
  }
 
  render() {
    return (
        <div style={{ marginTop: 10 }}>
            <h3 align="center">Update </h3>
            <form onSubmit={this.onSubmit}>
                <div className="form-group">
                    <label>Person Name:  </label>
                    <input type="text" className="form-control" value={this.state.person_name} onChange={this.onChangePersonName}/>
                </div>
                <div className="form-group">
                    <label>Business Email: </label>
                    <input type="text" 
                      className="form-control"
                      value={this.state.business_email}
                      onChange={this.onChangeBusinessEmail}
                      />
                </div>
                <div className="form-group">
                    <label>Phone Number: </label>
                    <input type="text" 
                      className="form-control"
                      value={this.state.business_phone_number}
                      onChange={this.onChangePhoneNumber}
                      />
                </div>
                <div className="form-group">
                    <input type="submit" 
                      value="Update " 
                      className="btn btn-primary"/>
                </div>
            </form>
        </div>
    )
  }
}