const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Business = new Schema({
  person_name: {
    type: String
  },
  business_email: {
    type: String
  },
  business_phone_number: {
    type: Number
  }
},{
    collection: 'business'
});

module.exports = mongoose.model('Business', Business);